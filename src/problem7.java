import java.util.Scanner;

public class problem7 {
    public static void main(String[] args) {
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan nilai angka : ");
        int nilai = scan.nextInt();

        //proses
        for (int i = 1; i <= nilai; i++){
            for (int j = 0; j < nilai - i ; j++) {
                System.out.print(" "); //menambahkan spasi

            }
            //menampilan simbol bintang
            for (int k = 1; k < i ; k++) {

                    System.out.print("* ");

            }

            System.out.println("*");
        }
    }
}
