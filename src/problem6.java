import java.util.Scanner;

public class problem6 {
    public static void main(String[] args) {
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("nilai x = ");
        int x = scan.nextInt();
        System.out.print("nilai n = ");
        int n = scan.nextInt();

        //proses
        double total=0;
        for (int i = 1; i <= n ; i++) {
             total = Math.pow(x,i);
        }
        System.out.println(x + " ^ "+ n +" = " + Math.round(total)); //menampilkan hasil perpangkatan
    }
}
