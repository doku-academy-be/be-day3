import java.util.Scanner;

public class problem5 {
    public static void main(String[] args) {
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan Kalimat : ");
        String kalimat = scan.nextLine();

        //proses
        String terbalik="";
        int panjang = kalimat.length();
        for (int i = panjang-1; i >=0 ; i--) {
            terbalik = terbalik + kalimat.charAt(i);
        }
       if (kalimat.equalsIgnoreCase(terbalik)){
           System.out.println("Palindrome"); //menampilkan hasil palindrome
         } else {
            System.out.println("Bukan Palindrome"); //menampilkan hasil bukan palindrome
        }
    }
}
