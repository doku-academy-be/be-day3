import java.util.Scanner;

public class problem4 {
    public static void main(String[] args) {
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan Bilangan: ");
        int bil = scan.nextInt();

        //proses
        int cek = 0;
        for(int i = 1; i<=bil;i++){
            if(bil % i == 0){
                cek++;
            }

        }
        if(cek<=2){
            System.out.println("Bilangan Prima"); //menampilkan hasil bilangan prima
        } else {
            System.out.println("Bukan Bilangan Prima"); //menampilkan hasil bukan bilangan prima
        }

    }
}
