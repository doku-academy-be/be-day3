import java.util.Scanner;

public class problem1 {
    public static void main(String[] args) {
        //input
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan kalimat :");
        String kalimat= scan.nextLine();

        //proses
        char[] vokal={'A','I','U','E','O','a','i','u','e','o'};
        int totalvokal=0;
        int totalkonsonan = 0;
        boolean isVokal;
        for (int i = 0; i < kalimat.length(); i++) {
            if ( String.valueOf(kalimat.charAt(i)).equals(" ")){
                continue;
            }
            isVokal = false;

            for (int j = 0; j < vokal.length; j++) {
                if (kalimat.charAt(i) == vokal[j] ){
                    totalvokal++;
                    isVokal = true;
                    break;
                }

            }
            if (!isVokal) {
                totalkonsonan++;
            }

        }

        //menampilkan hasil
        System.out.println("Total huruf vokal:" + totalvokal);
        System.out.println("Total huruf konsonan:" + totalkonsonan);
        System.out.println("Total karakter :" + (totalvokal + totalkonsonan));
    }
}
